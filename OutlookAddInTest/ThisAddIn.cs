﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using OutlookAddInTest.Tests;
using System.Windows.Forms;

namespace OutlookAddInTest
{
    public partial class ThisAddIn
    {
        List<IPhishTest> phishTests { get; set; }
        SettingsData settings;

        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            this.Application.NewMailEx += Application_NewMailEx;
            settings = new SettingsData();
            settings.LoadSettings();
            
            phishTests = new List<IPhishTest>();

            //temporarily allow real tests to be bypassed during UI improvements
            var fakeOnly = false;
            if (!fakeOnly)
            {
                //put tests in try/catch in case we hit API limits
                try
                {
                    phishTests.Add(new PhishTankPhishTest());
                }
                catch
                {
                    MessageBox.Show("Unable to load Phishtank scanning service", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                
                try
                {
                    phishTests.Add(new GooglePhishTest());
                }
                catch
                {
                    MessageBox.Show("Unable to load Google scanning service", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                phishTests.Add(new OutPhishDBTest());
                //add this for demo
                phishTests.Add(new FakeTempTest());
            }
            else
            {
                //only test for fake link to preserve API requests
                //while working on UI

                //reports 'http://digitaldominio.com/wellsfargo/' as phishing link
                phishTests.Add(new FakeTempTest());
            }
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            return new Ribbon1();
        }

        void Application_NewMailEx(string EntryIDCollection)
        {
            //reload settings in case of change
            settings.LoadSettings();

            var mailItem = GetMailItem(EntryIDCollection);
            var susLinks = GetSuspiciousLinks(GetMailLinks(mailItem));
            
            if(susLinks.Count > 0)
            {
                //reportOnClose indicates if the user clicked "report message"
                bool reportOnClose = false;
                if(!settings.defaultRemove)
                {
                    var form = new LinkForm();
                    form.InitEmailInfo(
                        mailItem.SenderName + " (" + mailItem.SenderEmailAddress + ")",
                        FormatMailSubject(mailItem.Subject)
                        );
                    form.InitLinks(susLinks);
                    form.ShowDialog();
                    reportOnClose = form.ReportOnClose;
                }
                
                if (reportOnClose || settings.defaultRemove)
                {
                    //disable links from archived phishing mail
                    mailItem.Body     = mailItem.Body.Replace("www.", "[link disabled] www_");
                    mailItem.HTMLBody = mailItem.HTMLBody.Replace("http:", "http_");
                    
                    Outlook.MAPIFolder inBox = (Outlook.MAPIFolder)this.Application.
                    ActiveExplorer().Session.GetDefaultFolder
                    (Outlook.OlDefaultFolders.olFolderInbox);
                    
                    try
                    {
                        mailItem.Move(inBox.Folders["ReportedMessages"]);
                    }
                    catch
                    {
                        try
                        {
                            //folder may not exist
                            Outlook.MAPIFolder newFolder =
                                (Outlook.MAPIFolder)inBox.Folders.Add("ReportedMessages",
                                Outlook.OlDefaultFolders.olFolderInbox);
                            
                            //retry
                            mailItem.Move(inBox.Folders["ReportedMessages"]);
                        }
                        catch { /*couldn't move */ }
                    }
                }
            }
        }

        private string FormatMailSubject(string unformattedSubject)
        {
            if (unformattedSubject != null && unformattedSubject.Length > 60)
            {
                unformattedSubject = unformattedSubject.Substring(0, 59);
                unformattedSubject += "...";
            }
            return unformattedSubject;
        }

        private Outlook.MailItem GetMailItem(string EntryIDCollection)
        {
            Outlook.NameSpace  outlookNS = Application.GetNamespace("MAPI");
            Outlook.MAPIFolder folder    = Application.Session.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox);
            Outlook.MailItem   mailItem  = (Outlook.MailItem)outlookNS.GetItemFromID(EntryIDCollection, folder.StoreID);
            return mailItem;
        }

        private List<string> GetMailLinks(Outlook.MailItem mailItem)
        {
            var links = new List<string>();

            if (mailItem != null)
            {
                if (mailItem.EntryID != null)
                {
                    var words = mailItem.Body.Split().ToList();
                    links.AddRange((from word in words where word.Contains("http") select word.TrimStart('<').TrimEnd('>')).Distinct().ToList());
                    //https will be found by http
                    //links.AddRange((from word in words where word.Contains("https") select word.TrimStart('<').TrimEnd('>')).Distinct().ToList());
                    links.AddRange((from word in words where word.Contains("ftp") select word.TrimStart('<').TrimEnd('>')).Distinct().ToList());
                }
            }

            return links;
        }

        private List<string> GetSuspiciousLinks(List<string> links)
        {
            var suspiciousLinks = new List<string>();
            
            foreach (var link in links)
                if (SuspiciousLink(link))
                    suspiciousLinks.Add(link);
            
            return suspiciousLinks;
        }

        private bool SuspiciousLink(string link)
        {
            foreach (var test in phishTests)
                if (test.IsPhish(link))
                    return true;
            
            return false;
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {

        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
