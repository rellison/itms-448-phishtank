﻿namespace OutlookAddInTest
{
    partial class LinkForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LinkForm));
            this.btnOk = new System.Windows.Forms.Button();
            this.linkBox = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.senderLbl = new System.Windows.Forms.Label();
            this.subjectLbl = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.moreInfoBtn = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(465, 330);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(162, 39);
            this.btnOk.TabIndex = 0;
            this.btnOk.Text = "Keep as Safe";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // linkBox
            // 
            this.linkBox.FormattingEnabled = true;
            this.linkBox.ItemHeight = 20;
            this.linkBox.Location = new System.Drawing.Point(12, 120);
            this.linkBox.Name = "linkBox";
            this.linkBox.Size = new System.Drawing.Size(615, 164);
            this.linkBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "Sender:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 20);
            this.label2.TabIndex = 3;
            this.label2.Text = "Subject:";
            // 
            // senderLbl
            // 
            this.senderLbl.AutoSize = true;
            this.senderLbl.Location = new System.Drawing.Point(84, 13);
            this.senderLbl.Name = "senderLbl";
            this.senderLbl.Size = new System.Drawing.Size(111, 20);
            this.senderLbl.TabIndex = 4;
            this.senderLbl.Text = "default sender";
            // 
            // subjectLbl
            // 
            this.subjectLbl.AutoSize = true;
            this.subjectLbl.Location = new System.Drawing.Point(84, 43);
            this.subjectLbl.Name = "subjectLbl";
            this.subjectLbl.Size = new System.Drawing.Size(113, 20);
            this.subjectLbl.TabIndex = 4;
            this.subjectLbl.Text = "default subject";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(297, 330);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(162, 39);
            this.button1.TabIndex = 5;
            this.button1.Text = "Remove Message";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(398, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Incoming email contained the following suspicious links:";
            // 
            // moreInfoBtn
            // 
            this.moreInfoBtn.Location = new System.Drawing.Point(129, 330);
            this.moreInfoBtn.Name = "moreInfoBtn";
            this.moreInfoBtn.Size = new System.Drawing.Size(162, 39);
            this.moreInfoBtn.TabIndex = 7;
            this.moreInfoBtn.Text = "More Info";
            this.moreInfoBtn.UseVisualStyleBackColor = true;
            this.moreInfoBtn.Click += new System.EventHandler(this.moreInfoBtn_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 290);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(269, 24);
            this.checkBox1.TabIndex = 8;
            this.checkBox1.Text = "Remove Messages Automatically";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // LinkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(639, 381);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.moreInfoBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.subjectLbl);
            this.Controls.Add(this.senderLbl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.linkBox);
            this.Controls.Add(this.btnOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LinkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Possible Phishing Links Detected";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ListBox linkBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label senderLbl;
        private System.Windows.Forms.Label subjectLbl;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button moreInfoBtn;
        private System.Windows.Forms.CheckBox checkBox1;
    }
}