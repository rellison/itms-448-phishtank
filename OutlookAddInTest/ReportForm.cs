﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Net;
using System.IO;

namespace OutlookAddInTest
{
    public partial class ReportForm : Form
    {
        private List<string> links;
        public ReportForm()
        {
            InitializeComponent();
            SetEmailLinks();
            linksListBox.DataSource = links;
            SelectAllLinks();

            //if there aren't links in the email selected, reduce functionality
            if (links.Count == 0)
            {
                btnClear.Enabled        = false;
                btnSelectAll.Enabled    = false;
                btnReport.Enabled       = false;
                List<string> noLinks    = new List<string>();
                noLinks.Add("Selected Email Contains no Links");
                linksListBox.DataSource = noLinks;
                linksListBox.Enabled    = false;
            }
        }

        //Some source of this code from:
        //http://stackoverflow.com/questions/10935611/retrieve-current-email-body-in-outlook
        private void SetEmailLinks()
        {
            links = new List<string>();

            //find links within currently-selected email
            Outlook._Application oApp = new Outlook.Application();
            if (oApp.ActiveExplorer().Selection.Count > 0)
            {
                Object selObject = oApp.ActiveExplorer().Selection[1];

                if (selObject is Outlook.MailItem)
                {
                    try
                    {
                        Outlook.MailItem mailItem = (selObject as Outlook.MailItem);

                        var words = mailItem.Body.Split().ToList();
                        links.AddRange((from word in words where word.Contains("http") select word.TrimStart('<').TrimEnd('>')).Distinct().ToList());
                        links.AddRange((from word in words where word.Contains("ftp") select word.TrimStart('<').TrimEnd('>')).Distinct().ToList());
                    }
                    catch { } //avoid issues if user denies access to addin
                }
            }
        }

        private void SelectAllLinks()
        {
            for (int i = 0; i < linksListBox.Items.Count; i++)
                linksListBox.SetItemChecked(i, true);
        }

        private void DeselectAllLinks()
        {
            for (int i = 0; i < linksListBox.Items.Count; i++)
                linksListBox.SetItemChecked(i, false);
        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            SelectAllLinks();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            DeselectAllLinks();
        }

        //This method makes use of information from the following link:
        //http://stackoverflow.com/questions/943852/how-to-send-an-https-get-request-in-c-sharp
        private void btnReport_Click(object sender, EventArgs e)
        {
            //build report link
            var selectedItems    = linksListBox.CheckedItems;
            int numLinksReported = selectedItems.Count;
            string reportLink    = "http://www.outphish.com/report.php?";
            int linkNum = 1;
            
            foreach (var item in selectedItems){
                if (linkNum > 1)
                {
                    reportLink += "&";
                }
                reportLink += "link" + linkNum + "=" + item.ToString();
                linkNum++;
            }

            //submit link
            if (numLinksReported > 0)
            {
                try
                {
                    System.Diagnostics.Process.Start(reportLink);
                    /*
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(reportLink);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    Stream resStream = response.GetResponseStream();
                    MessageBox.Show((numLinksReported + " phishing links have been reported to OutPhish."), "Outphish Report Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    */
                }
                catch
                {
                    MessageBox.Show("Error reporting links.", "Outphish Report Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                
                this.Close();
            }
            else
            {
                MessageBox.Show("No phishing links have been selected.", "Outphish Report Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}