﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace OutlookAddInTest
{
    [Serializable]
    public class SettingsData
    {
        public bool   defaultRemove    { get; set; }
        public string databaseUrl      { get; set; }
        public string databaseUsername { get; set; }
        public string databasePassword { get; set; }

        public SettingsData()
        {
            defaultRemove    = false;
            databaseUrl      = "outphish.com";
            databaseUsername = "anonymous";
            databasePassword = "outphish";
        }

        //Save settings to settings file
        public void SaveSettings()
        {
            try
            {
                XmlDocument xmlDocument    = new XmlDocument();
                XmlSerializer serializer   = new XmlSerializer(typeof(SettingsData));
                using (MemoryStream stream = new MemoryStream())
                {
                    serializer.Serialize(stream, this);
                    stream.Position = 0;
                    xmlDocument.Load(stream);
                    xmlDocument.Save("outphish_settings.xml");
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        //Load settings from settings file
        public void LoadSettings()
        {
            try
            {
                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load("outphish_settings.xml");
                string xmlString = xmlDocument.OuterXml;
                SettingsData tempSettings = null;

                using (StringReader read = new StringReader(xmlString))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(SettingsData));
                    using (XmlReader reader = new XmlTextReader(read))
                    {
                        tempSettings = (SettingsData)serializer.Deserialize(reader);
                        reader.Close();
                    }

                    read.Close();
                }

                if (tempSettings == null)
                {
                    tempSettings = new SettingsData();
                }
                else
                {
                    defaultRemove    = tempSettings.defaultRemove;
                    databaseUrl      = tempSettings.databaseUrl;
                    databaseUsername = tempSettings.databaseUsername;
                    databasePassword = tempSettings.databasePassword;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
