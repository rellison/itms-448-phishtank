﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddInTest.Tests
{
    class GooglePhishTest : IPhishTest
    {
        public bool CheckLink(string link)
        {
            using (var webClient = new System.Net.WebClient())
            {
                try
                {   
                    var dataurl = "https://sb-ssl.google.com/safebrowsing/api/lookup?client=api&apikey=ABQIAAAAaeb-rxEm5rwbZXeX-TNEZxT6yN3ojHOlLpDJ4TXApTPi8uV0gw&appver=1.0&pver=3.0&url=";
                    dataurl += link;  
                    webClient.DownloadFile(dataurl, "goog_resp.txt");

                    //load phishing urls
                    using (var file = new System.IO.StreamReader("goog_resp.txt"))
                    {
                        try
                        {
                            string response = file.ReadLine();

                            //file exists but not valid
                            if (response == null) return false;

                            //see if this is a bad vs. okay link
                            if (response.Contains("malware") || response.Contains("phishing"))
                                return true;
                            else
                                return false;
                        }
                        catch { }
                    }
                }
                catch { } 
            }
            
            return false;
        }
        
        public bool IsPhish(string link)
        {
            return CheckLink(link);
        }
    }
}
//git change