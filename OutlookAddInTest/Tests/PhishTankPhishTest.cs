﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddInTest.Tests
{
    class PhishTankPhishTest : IPhishTest
    {
        List<string> phishingUrls { get; set; }
        DateTime     lastUpdated  { get; set; }

        public PhishTankPhishTest()
        {
            phishingUrls = new List<string>();
            UpdatePhishingUrls();
        }

        private void UpdatePhishingUrls()
        {
            using (var webClient = new System.Net.WebClient())
            {
                //try to update saved file
                try
                {
                    var dataurl = @"http://data.phishtank.com/data/6e93714444ef8f815abde4a105b00b28d4c0c8fe7fe8487c16a7c1de1351f809/online-valid.csv";
                    webClient.DownloadFile(dataurl, "phishLinks.txt");
                }
                catch { }

                //load phishing urls
                using (var file = new System.IO.StreamReader("phishLinks.txt"))
                {
                    try
                    {
                        //eliminate first line
                        string entry = file.ReadLine();
                        if (entry == null) return; //could not read anything from file...should throw error later (?)
                        
                        phishingUrls.Clear();

                        //load phishing urls from file
                        while ((entry = file.ReadLine()) != null)
                        {
                            var line = entry.Split(',');
                            if(line.Length > 1)
                                phishingUrls.Add(line[1]);
                        }

                        if (phishingUrls.Count > 100)
                            lastUpdated = DateTime.Now;
                    }
                    catch { }
                }
            }
        }

        public bool IsPhish(string link)
        {
            var timeSinceLastUpdate = DateTime.Now - lastUpdated;
            if (timeSinceLastUpdate.Hours > 0 || timeSinceLastUpdate.Days > 0 || phishingUrls.Count < 100)
                UpdatePhishingUrls();

            //is link in phishingUrls?
            return phishingUrls.Contains(link);  
        }
    }
}
//git change