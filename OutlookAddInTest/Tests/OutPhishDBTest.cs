﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutlookAddInTest.Tests
{
    class OutPhishDBTest : IPhishTest
    {
        public bool CheckLink(string link)
        {
            using (var webClient = new System.Net.WebClient())
            {
                try
                {
                    var dataurl = "http://www.outphish.com/phishcheck?url=";
                    dataurl += link;
                    webClient.DownloadFile(dataurl, "outphishDB_resp.txt");

                    //load phishing urls
                    using (var file = new System.IO.StreamReader("outphishDB_resp.txt"))
                    {
                        try
                        {
                            string response = file.ReadLine();

                            //file exists but not valid
                            if (response == null) return false;

                            //see if this is a bad vs. okay link
                            if (response.Contains("phish"))
                                return true;
                            else
                                return false;
                        }
                        catch { }
                    }
                }
                catch { }
            }

            return false;
        }
        public bool IsPhish(string link)
        {
            return CheckLink(link);
        }
    }
}
//git change