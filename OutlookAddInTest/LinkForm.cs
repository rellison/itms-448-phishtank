﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddInTest
{
    public partial class LinkForm : Form
    {
        public bool ReportOnClose { get; set; }

        public LinkForm()
        {
            InitializeComponent();
        }

        public void InitLinks(List<string> links)
        {
            linkBox.DataSource = links;
        }

        public void InitEmailInfo(string sender, string subject)
        {
            subjectLbl.Text = subject;
            senderLbl.Text  = sender;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            //for now, just close the form (no marking as safe yet)
            ReportOnClose = false;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //for now, just move the mail to spam (no reporting)
            ReportOnClose = true;

            //save any changes to settings
            SettingsData sd = new SettingsData();
            sd.LoadSettings();
            sd.defaultRemove = checkBox1.Checked;
            sd.SaveSettings();

            this.Close();
        }

        private void moreInfoBtn_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.outphish.com");
        }
    }
}
