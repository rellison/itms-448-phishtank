﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OutlookAddInTest
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            var settings = new SettingsData();
            settings.LoadSettings();
            InitializeComponent();

            databaseTxt.Text = settings.databaseUrl;
            usernameTxt.Text = settings.databaseUsername;
            passwordTxt.Text = settings.databasePassword;
            defaultRemoveChk.Checked = settings.defaultRemove;
            btnSave.Enabled = false;
        }

        private void enableSaveBtn(object sender, EventArgs e)
        {
            btnSave.Enabled = true;
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://www.outphish.com");
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var settings = new SettingsData();

            settings.databaseUrl      = databaseTxt.Text;
            settings.databaseUsername = usernameTxt.Text;
            settings.databasePassword = passwordTxt.Text;
            settings.defaultRemove    = defaultRemoveChk.Checked;

            settings.SaveSettings();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
